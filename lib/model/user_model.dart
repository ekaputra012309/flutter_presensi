class UserRegistration {
  final String email;
  final String password;
  final String confirmPassword;
  final String deviceImei;
  final String deviceName;

  UserRegistration({
    required this.email,
    required this.password,
    required this.confirmPassword,
    required this.deviceImei,
    required this.deviceName,
  });

  Map<String, dynamic> toJson() {
    return {
      'email': email,
      'password': password,
      'confirm_password': confirmPassword,
      'device_imei': deviceImei,
      'device_name': deviceName,
    };
  }
}
