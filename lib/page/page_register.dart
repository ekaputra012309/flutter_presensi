import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:presensi/page/page_login.dart';

import '../model/user_model.dart';
import 'dart:async';

import 'package:flutter/services.dart';
import 'package:device_information/device_information.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({Key? key}) : super(key: key);

  @override
  State<RegisterPage> createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final TextEditingController emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  final TextEditingController retypePasswordController =
      TextEditingController();
  bool _passwordVisible = false;
  bool _retypePasswordVisible = false;
  String _imeiNo = "", _deviceName = "";

  @override
  void initState() {
    super.initState();
    initPlatformState();
  }

  Future<void> initPlatformState() async {
    late String platformVersion, imeiNo = '', deviceName = '';
    // Platform messages may fail,
    // so we use a try/catch PlatformException.
    try {
      platformVersion = await DeviceInformation.platformVersion;
      imeiNo = await DeviceInformation.deviceIMEINumber;
      deviceName = await DeviceInformation.deviceName;
    } on PlatformException catch (e) {
      platformVersion = '${e.message}';
    }
    if (!mounted) return;

    setState(() {
      _imeiNo = imeiNo;
      _deviceName = deviceName;
    });
  }

  Future<void> _handleSubmit() async {
    // Ensure that device information is available
    if (_imeiNo == null || _deviceName == null) {
      // Wait for a short duration and then try again
      await Future.delayed(Duration(seconds: 2));
      await _handleSubmit();
      return;
    }

    final email = emailController.text;
    final password = passwordController.text;
    final confirmPassword = retypePasswordController.text;

    if (!_isFormValid(email, password, confirmPassword)) {
      return; // Stop if the form is not valid
    }

    final userRegistration = UserRegistration(
      email: email,
      password: password,
      confirmPassword: confirmPassword,
      deviceImei: _imeiNo,
      deviceName: _deviceName,
    );

    // print(userRegistration.toJson());
    // Add your registration code here
    final dio = Dio();

    dio.interceptors
        .add(LogInterceptor(request: true, responseBody: true, error: true));

    try {
      final response = await dio.post(
        'http://192.168.17.58:8080/api/register',
        data: userRegistration.toJson(),
      );

      if (response.statusCode == 200) {
        _showErrorSnackBar('Registration successful');
      } else {
        _showErrorSnackBar('Registration failed. Please try again.');
      }
    } catch (error) {
      if (error is DioError) {
        if (error.response != null) {
          // Handle server error based on response status code or message
          final errorMessage =
              error.response?.data['message'] ?? 'Server error';
          _showErrorSnackBar(errorMessage);
        } else {
          _showErrorSnackBar('An error occurred. Please try again.');
        }
      } else if (error is SocketException && error.osError?.errorCode == 110) {
        _showErrorSnackBar('Connection timed out. Please check your network.');
      } else {
        _showErrorSnackBar('An unknown error occurred.');
      }
    }
  }

  bool _isFormValid(String email, String password, String confirmPassword) {
    if (email.isEmpty || !isValidEmail(email)) {
      _showErrorSnackBar('Invalid email format');
      return false;
    } else if (password.isEmpty) {
      _showErrorSnackBar('Password is required');
      return false;
    } else if (password.length < 8) {
      _showErrorSnackBar('Password must be at least 8 characters');
      return false;
    } else if (confirmPassword.isEmpty) {
      _showErrorSnackBar('Retype Password is required');
      return false;
    } else if (confirmPassword.length < 8) {
      _showErrorSnackBar('Retype Password must be at least 8 characters');
      return false;
    } else if (password != confirmPassword) {
      _showErrorSnackBar('Passwords do not match');
      return false;
    }
    return true; // Form is valid
  }

  bool isValidEmail(String email) {
    final emailRegExp = RegExp(r'^[\w-]+(\.[\w-]+)*@[\w-]+(\.[\w-]+)+$');
    return emailRegExp.hasMatch(email);
  }

  void _showErrorSnackBar(String message) {
    final snackBar = SnackBar(content: Text(message));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  @override
  Widget build(BuildContext context) {
    const hijau = Color.fromARGB(255, 39, 202, 188);

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        leading: IconButton(
          icon: const Icon(Icons.arrow_back_ios_new_rounded),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: SafeArea(
        child: Center(
          child: Padding(
            padding: const EdgeInsets.all(24.0),
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Center(
                    child: Image.asset(
                      'assets/login-amico.png',
                      width: 280.0,
                    ),
                  ),
                  const SizedBox(
                    height: 16.0,
                  ),
                  const Text(
                    'Daftar',
                    style: TextStyle(
                      fontSize: 36.0,
                      fontWeight: FontWeight.bold,
                      color: hijau,
                    ),
                  ),
                  const SizedBox(height: 16.0),
                  TextFormField(
                    controller: emailController,
                    decoration: InputDecoration(
                      labelText: 'Email',
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10.0),
                        borderSide: const BorderSide(
                          color: hijau,
                          width: 2.0,
                        ),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10.0),
                        borderSide: const BorderSide(
                          color: hijau,
                          width: 2.0,
                        ),
                      ),
                      errorBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10.0),
                        borderSide: const BorderSide(
                          color: Colors.red,
                          width: 2.0,
                        ),
                      ),
                    ),
                    keyboardType: TextInputType.emailAddress,
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Email is required';
                      }
                      return null;
                    },
                  ),
                  const SizedBox(height: 16.0),
                  TextFormField(
                    controller: passwordController,
                    decoration: InputDecoration(
                      labelText: 'Password',
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10.0),
                        borderSide: const BorderSide(
                          color: hijau,
                          width: 2.0,
                        ),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10.0),
                        borderSide: const BorderSide(
                          color: hijau,
                          width: 2.0,
                        ),
                      ),
                      errorBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10.0),
                        borderSide: const BorderSide(
                          color: Colors.red,
                          width: 2.0,
                        ),
                      ),
                      suffixIcon: IconButton(
                        icon: Icon(
                          _passwordVisible
                              ? Icons.visibility
                              : Icons.visibility_off,
                          color: Colors.grey,
                        ),
                        onPressed: () {
                          setState(() {
                            _passwordVisible = !_passwordVisible;
                          });
                        },
                      ),
                    ),
                    obscureText: !_passwordVisible,
                    keyboardType: TextInputType.visiblePassword,
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Password is required';
                      }
                      return null;
                    },
                  ),
                  const SizedBox(height: 16.0),
                  TextFormField(
                    controller: retypePasswordController,
                    decoration: InputDecoration(
                      labelText: 'Retype Password',
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10.0),
                        borderSide: const BorderSide(
                          color: hijau,
                          width: 2.0,
                        ),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10.0),
                        borderSide: const BorderSide(
                          color: hijau,
                          width: 2.0,
                        ),
                      ),
                      errorBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10.0),
                        borderSide: const BorderSide(
                          color: Colors.red,
                          width: 2.0,
                        ),
                      ),
                      suffixIcon: IconButton(
                        icon: Icon(
                          _retypePasswordVisible
                              ? Icons.visibility
                              : Icons.visibility_off,
                          color: Colors.grey,
                        ),
                        onPressed: () {
                          setState(() {
                            _retypePasswordVisible = !_retypePasswordVisible;
                          });
                        },
                      ),
                    ),
                    obscureText: !_retypePasswordVisible,
                    keyboardType: TextInputType.visiblePassword,
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Retype Password is required';
                      } else if (value.length < 8 ||
                          value != passwordController.text) {
                        return 'Invalid input or passwords do not match';
                      }
                      return null;
                    },
                  ),
                  const SizedBox(height: 16.0),
                  SizedBox(
                    width: double.infinity,
                    child: ElevatedButton(
                      onPressed: _handleSubmit,
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all<Color>(
                          hijau,
                        ),
                      ),
                      child: const Padding(
                        padding: EdgeInsets.symmetric(
                          vertical: 16.0,
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Icon(
                              Icons.login_rounded,
                              size: 18.0,
                              color: Colors.white,
                            ),
                            SizedBox(width: 8.0),
                            Text(
                              'Sign Up',
                              style: TextStyle(
                                  fontSize: 18.0, color: Colors.white),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(height: 16.0),
                  Row(
                    children: [
                      const Text('Sudah punya akun ? '),
                      TextButton(
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => const LoginPage()));
                        },
                        child: const Text(
                          'Masuk',
                          style: TextStyle(
                            color: hijau,
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
